<?php

require_once 'autoload.php';

use Service\Composter;
use Service\GlassRecycler;
use Service\Incinerator;
use Service\MetalRecycler;
use Service\PaperRecycler;
use Service\PlasticRecycler;
use Waste\Glass;
use Waste\Metal;
use Waste\Organic;
use Waste\Other;
use Waste\Paper;
use Waste\PC;
use Waste\PEHD;
use Waste\PET;
use Waste\PVC;

//get json data for service and waste
$data = json_decode(file_get_contents('Data/data.json'));

//get json co2 data
$co2data = json_decode(file_get_contents('Data/co2.json'));

//init service
$glassService = new GlassRecycler(0);
$composter = new Composter(0);
$incinerator = new Incinerator(0);
$metalService = new MetalRecycler(0);
$paperService = new PaperRecycler(0);
$plasticService = new PlasticRecycler(0);

//init waste with co2 json data
$glassWaste = new Glass(0, $co2data->verre->incineration, $co2data->verre->recyclage);
$organicWaste = new Organic(0, $co2data->organique->incineration, $co2data->organique->compostage);
$otherWaste = new Other(0, $co2data->autre->incineration, 0);
$metalWaste = new Metal(0, $co2data->metaux->incineration, $co2data->metaux->recyclage);
$paperWaste = new Paper(0, $co2data->papier->incineration, $co2data->papier->recyclage);
$pcWaste = new PC(0, $co2data->plastiques->PC->incineration, $co2data->plastiques->PC->recyclage);
$pehdWaste = new PEHD(0, $co2data->plastiques->PEHD->incineration, $co2data->plastiques->PEHD->recyclage);
$petWaste = new PET(0, $co2data->plastiques->PET->incineration, $co2data->plastiques->PET->recyclage);
$pvcWaste = new PVC(0, $co2data->plastiques->PVC->incineration, $co2data->plastiques->PVC->recyclage);

//add capacity for each service in the data json
foreach ($data->services as $service)
{
    switch ($service->type)
    {
        case 'recyclageVerre' :
            $glassService->addCapacity($service->capacite);
            break;

        case 'composteur' :
            $composter->addCapacity($service->capacite * $service->foyers);
            break;

        case 'incinerateur' :
            $incinerator->addCapacity($service->ligneFour * $service->capaciteLigne);
            break;

        case 'recyclagePlastique' :
            $plasticService->addCapacity($service->capacite);
            break;

        case 'recyclagePapier' :
            $paperService->addCapacity($service->capacite);
            break;

        case 'recyclageMetaux' :
            $metalService->addCapacity($service->capacite);
            break;
    }
}

//add quantity of waste for each waste in data json
foreach ($data->quartiers as $waste)
{
    $glassWaste->addWasteQuantity($waste->papier);
    $organicWaste->addWasteQuantity($waste->organique);
    $otherWaste->addWasteQuantity($waste->autre);
    $metalWaste->addWasteQuantity($waste->metaux);
    $paperWaste->addWasteQuantity($waste->papier);
    $pcWaste->addWasteQuantity($waste->plastiques->PC);
    $pehdWaste->addWasteQuantity($waste->plastiques->PEHD);
    $petWaste->addWasteQuantity($waste->plastiques->PET);
    $pvcWaste->addWasteQuantity($waste->plastiques->PVC);
}

$glassCo2 = $glassService->recycleGlass($glassWaste);
$organicCo2 = $composter->recycleOrganic($organicWaste);
$otherCo2 = $incinerator->incinerate($otherWaste);
$metalCo2 = $metalService->recycleMetal($metalWaste);
$paperCo2 = $paperService->recyclePaper($paperWaste);
$pcCo2 = $plasticService->recyclePC($pcWaste);
$pehdCo2 = $plasticService->recyclePEHD($pehdWaste);
$petCo2 = $plasticService->recyclePET($petWaste);
$pvcCo2 = $plasticService->recyclePVC($pvcWaste);

$totalCo2 = 0;

$totalCo2 += $glassCo2;
$totalCo2 += $organicCo2;
$totalCo2 += $otherCo2;
$totalCo2 += $metalCo2;
$totalCo2 += $paperCo2;
$totalCo2 += $pcCo2;
$totalCo2 += $pehdCo2;
$totalCo2 += $petCo2;
$totalCo2 += $pvcCo2;

$plasticCo2 = 0;

$plasticCo2 += $pcCo2;
$plasticCo2 += $pehdCo2;
$plasticCo2 += $petCo2;
$plasticCo2 += $pvcCo2;

echo 'total co2 rejected : '.$totalCo2;
echo "<br>";
echo 'Co2 rejected by Glass Recycler : '.$glassCo2;
echo "<br>";
echo 'Co2 rejected by Metal Recycler : '.$metalCo2;
echo "<br>";
echo 'Co2 rejected by Composter : '.$organicCo2;
echo "<br>";
echo 'Co2 rejected by Paper Recycler : '.$paperCo2;
echo "<br>";
echo 'Co2 rejected by Incinerator : '.$otherCo2;
echo "<br>";
echo 'Co2 rejected by Plastic Recycler : '.$plasticCo2;