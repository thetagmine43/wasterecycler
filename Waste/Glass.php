<?php

namespace Waste;

class Glass implements WasteInterface
{
    private float $quantity;

    private int $incinerateCo2;

    private int $recycleCo2;

    public function __construct(float $quantity, int $incinerateCo2, int $recycleCo2)
    {
        $this->quantity = $quantity;
        $this->incinerateCo2 = $incinerateCo2;
        $this->recycleCo2 = $recycleCo2;
    }

    /**
     * @return float
     */
    public function getWasteQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setWasteQuantity(float $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param float $quantity
     */
    public function removeWasteQuantity(float $quantity)
    {
        $this->quantity -= $quantity;
    }

    public function getIncinerateCo2(): int
    {
        return $this->incinerateCo2;
    }

    public function getRecycleCo2(): int
    {
        return $this->recycleCo2;
    }

    /**
     * @param float $quantity
     */
    public function addWasteQuantity(float $quantity)
    {
        $this->quantity += $quantity;
    }
}