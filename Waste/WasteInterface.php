<?php

namespace Waste;

interface WasteInterface
{
    public function getWasteQuantity():float;

    public function setWasteQuantity(float $quantity);

    public function removeWasteQuantity(float $quantity);

    public function addWasteQuantity(float $quantity);

    public function getIncinerateCo2():int;

    public function getRecycleCo2():int;
}