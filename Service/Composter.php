<?php

namespace Service;

use Waste\Organic;

class Composter extends AbstractService
{
    /**
     * @param Organic $waste
     * @return float
     */
    public function recycleOrganic(Organic $waste): float
    {
        return parent::recycle($waste);
    }
}