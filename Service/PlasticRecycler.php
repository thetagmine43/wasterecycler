<?php

namespace Service;

use Waste\PC;
use Waste\PEHD;
use Waste\PET;
use Waste\PVC;

class PlasticRecycler extends AbstractService
{
    /**
     * @param PC $waste
     * @return float
     */
    public function recyclePC(PC $waste): float
    {
        return parent::recycle($waste);
    }

    /**
     * @param PEHD $waste
     * @return float
     */
    public function recyclePEHD(PEHD $waste): float
    {
        return parent::recycle($waste);
    }

    /**
     * @param PET $waste
     * @return float
     */
    public function recyclePET(PET $waste): float
    {
        return parent::recycle($waste);
    }

    /**
     * @param PVC $waste
     * @return float
     */
    public function recyclePVC(PVC $waste): float
    {
        return parent::recycle($waste);
    }
}