<?php

namespace Service;

use Waste\WasteInterface;

abstract class AbstractService
{

    private float $capacity;

    public function __construct(float $capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @param WasteInterface $waste
     * @return float
     */
    protected function recycle(WasteInterface $waste): float
    {
        $quantity = $waste->getWasteQuantity();

        if (($this->getCapacity() - $quantity) < 0)
        {
            $proceedQuantity = $quantity - $this->getCapacity();
            $co2 = $waste->getRecycleCo2();
            $proceedCo2 = $co2 * $this->getCapacity();
            $waste->removeWasteQuantity($this->getCapacity());
            $this->setCapacity(0);
        } else {
            $proceedQuantity = $this->getCapacity() - $quantity;
            $co2 = $waste->getRecycleCo2();
            $proceedCo2 = $co2 * $quantity;
            $this->removeCapacity($quantity);
            $waste->setWasteQuantity(0);
        }

        return $proceedCo2;
    }

    /**
     * @return float
     */
    public function getCapacity(): float
    {
        return $this->capacity;
    }

    /**
     * @param float $capacity
     */
    public function setCapacity(float $capacity): void
    {
        $this->capacity = $capacity;
    }

    public function removeCapacity(float $capacity): void
    {
        $this->capacity -= $capacity;
    }

    public function addCapacity(float $capacity): void
    {
        $this->capacity += $capacity;
    }

}