<?php

namespace Service;

use Waste\Glass;

class GlassRecycler extends AbstractService
{
    /**
     * @param Glass $waste
     * @return float
     */
    public function recycleGlass(Glass $waste): float
    {
        return parent::recycle($waste);

    }
}