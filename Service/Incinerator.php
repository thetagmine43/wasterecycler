<?php

namespace Service;

use Waste\WasteInterface;

class Incinerator extends AbstractService
{
    public function incinerate(WasteInterface $waste)
    {
        $quantity = $waste->getWasteQuantity();

        if (($this->getCapacity() - $quantity) < 0)
        {
            $proceedQuantity = $quantity - $this->getCapacity();
            $co2 = $waste->getIncinerateCo2();
            $proceedCo2 = $co2 * $this->getCapacity();
            $waste->removeWasteQuantity($this->getCapacity());
            $this->setCapacity(0);
        } else {
            $proceedQuantity = $this->getCapacity() - $quantity;
            $co2 = $waste->getIncinerateCo2();
            $proceedCo2 = $co2 * $quantity;
            $this->removeCapacity($quantity);
            $waste->setWasteQuantity(0);
        }

        return $proceedCo2;
    }
}