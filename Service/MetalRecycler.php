<?php

namespace Service;

use Waste\Metal;

class MetalRecycler extends AbstractService
{
    /**
     * @param Metal $waste
     * @return float
     */
    public function recycleMetal(Metal $waste): float
    {
        return parent::recycle($waste);

    }
}