<?php

namespace Service;

use Waste\Paper;

class PaperRecycler extends AbstractService
{
    /**
     * @param Paper $waste
     * @return float
     */
    public function recyclePaper(Paper $waste): float
    {
        return parent::recycle($waste);
    }
}